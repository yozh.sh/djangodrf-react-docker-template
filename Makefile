# ONLY FOR DEVELOPMENT

.DEFAULT_GOAL := help
DOCKERFILE=docker-compose.dev.yml

dc-build:
	docker-compose -f $(DOCKERFILE) build

dc-up:
	docker-compose -f $(DOCKERFILE) up


# DJANGO BACKEND

dc-django-up:
	docker-compose -f $(DOCKERFILE) up django_app

dc-django-build:
	docker-compose -f $(DOCKERFILE) build django_app

dc-django-collectstatic:
	docker-compose -f $(DOCKERFILE) run django_app python manage.py collectstatic --no-input

dc-django-makemigrations:
	docker-compose -f $(DOCKERFILE) run django_app python manage.py makemigrations

dc-django-migrate:
	docker-compose -f $(DOCKERFILE) run django_app python manage.py migrate

dc-django-makeandmigrate: dc-backend-makemigrations dc-backend-migrate

dc-django-createsuperuser:
	docker-compose -f $(DOCKERFILE) run django_app python manage.py createsuperuser

dc-django-shell:
	docker-compose -f docker-compose.dev.yml run django_app python manage.py shell

dc-django-exec:
	docker-compose -f $(DOCKERFILE) exec django_app /bin/bash

